{$DEFINE WEB_UNIGUI} // Diretiva Interna

//{$define UNIGUI_VCL} // Diretiva Gera EXE
{$define UNIGUI_DLL} // Diretiva Gera DLL
//{$define UNIGUI_SERVICE} // Diretiva Service

  {$IFDEF UNIGUI_VCL}
    program {$E exe}
  {$ENDIF}
  {$IFDEF UNIGUI_DLL}
    library {$E dll}
  {$ENDIF}
  {$IFDEF UNIGUI_SERVICE}
    program
  {$ENDIF}

  VersionOldJsonToDelphi;

uses
  {$IFDEF UNIGUI_VCL}
  Forms,
  {$ENDIF }
  {$IFDEF UNIGUI_DLL}
  uniGUIISAPI,
  {$ENDIF }
  {$IFDEF UNIGUI_SERVICE}
  SvcMgr,
  {$ENDIF }
  ServerModule in 'ServerModule.pas' {UniServerModule: TUniGUIServerModule},
  MainModule in 'MainModule.pas' {UniMainModule: TUniGUIMainModule},
  Main in 'Main.pas' {MainForm: TUniForm},
  uFrmWebNavigate in 'uFrmWebNavigate.pas' {frmWebNavigate: TUniForm},
  uFrmGenerateUnit in 'uFrmGenerateUnit.pas' {frmGenerateUnit: TUniForm},
  Converters in '..\Converters.pas',
  Pkg.Json.Mapper.UniGui in '..\Pkg.Json.Mapper.UniGui.pas',
  Writers in '..\Writers.pas',
  FS.Sistema in 'classes\FS.Sistema.pas';

{$R *.res}

{$ifndef UNIGUI_VCL}
exports
  GetExtensionVersion,
  HttpExtensionProc,
  TerminateExtension;
{$endif}

begin
  {$IFDEF DEBUG}
    ReportMemoryLeaksOnShutdown  := True;
  {$ENDIF}

  {$IFDEF UNIGUI_VCL}
    Application.Initialize;
    TUniServerModule.Create(Application);
    Application.Title := 'Json To Delphi Class';
    Application.Run;
  {$ENDIF}

  {$IFDEF UNIGUI_SERVICE}
    if not Application.DelayInitialize or Application.Installing then
      Application.Initialize;
    Application.CreateForm(TJsonToDelphiService, JsonToDelphiService);
  Application.Run;
  {$ENDIF}
end.
