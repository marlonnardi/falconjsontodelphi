unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, uniGUITypes, uniGUIAbstractClasses, uniGUIClasses, uniGUIRegClasses,
  uniGUIForm, uniLabel, uniGUIBaseClasses, uniPanel, uniMemo, uniHTMLFrame,
  uniSplitter, Pkg.Json.Mapper.UniGui, Converters, uniRadioGroup, uniButton,
  uniBitBtn, UniFSButton, uniImage, UniFSConfirm;

type
  TMainForm = class(TUniForm)
    pnlMaster: TUniPanel;
    pnlTop: TUniPanel;
    lblTitle: TUniLabel;
    lblSubTitle: TUniLabel;
    lblDeveloper: TUniLabel;
    memJson: TUniMemo;
    lbl1: TUniLabel;
    pnlControl: TUniPanel;
    lbl2: TUniLabel;
    lbl3: TUniLabel;
    lbl4: TUniLabel;
    lbl5: TUniLabel;
    spl1: TUniSplitter;
    lblDoacao: TUniLabel;
    grpConfig: TUniRadioGroup;
    btnGenerate: TUniFSButton;
    btnConfig: TUniFSButton;
    btnValidate: TUniFSButton;
    btn1: TUniFSButton;
    lblVersion: TUniLabel;
    UniLabel1: TUniLabel;
    UniLabel2: TUniLabel;
    lbl6: TUniLabel;
    UniLabel3: TUniLabel;
    UniLabel4: TUniLabel;
    imgFalconSistemas: TUniImage;
    Confirm: TUniFSConfirm;
    lblJsonToPascal: TUniLabel;
    lbl7: TUniLabel;
    procedure UniFormAfterShow(Sender: TObject);
    procedure UniFormClose(Sender: TObject; var Action: TCloseAction);
    procedure UniFormAjaxEvent(Sender: TComponent; EventName: string;
      Params: TUniStrings);
    procedure btnGenerateClick(Sender: TObject);
    procedure btnConfigClick(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure lbl7Click(Sender: TObject);
  private
    { Private declarations }
    jm : TPkgJsonMapper;

    procedure DefineRegrasLayout;
    procedure AlinhamentoCenter;
  public
    { Public declarations }
  end;

function MainForm: TMainForm;

implementation

{$R *.dfm}

uses
  uniGUIVars, MainModule, uniGUIApplication, uFrmWebNavigate, uFrmGenerateUnit,
  FS.Sistema, ServerModule;

function MainForm: TMainForm;
begin
  Result := TMainForm(UniMainModule.GetFormInstance(TMainForm));
end;

{ TMainForm }

procedure TMainForm.AlinhamentoCenter;
begin
  pnlMaster.Top    := Self.Top;
  pnlMaster.Height := Self.Height;
  pnlMaster.Left   := (Self.Width div 2) - (pnlMaster.Width div 2);
end;

procedure TMainForm.btn1Click(Sender: TObject);
begin
  Confirm.Alert('About Donate',
    '*Help keep domain https://jsontodelphi.com online.</br>'+
    '*Help keep hosting server online. </br> '+
    '*Help get new features. </br></br> '+

    'All the amount collected will be kept for these purposes and others if they arise. </br></br>'+
    'suporte@falconsistemas.com.br </br></br>',
    'fa fa-handshake-o',TTypeColor.blue, TTheme.supervan);
end;

procedure TMainForm.btnConfigClick(Sender: TObject);
begin
  grpConfig.Visible := not(grpConfig.Visible);
end;

procedure TMainForm.btnGenerateClick(Sender: TObject);
var
  vJson : string;
begin
  vJson := Trim(memJson.Lines.Text);

  if Length(vJson) <= 4 then
    Exit;

  try
    vJson := TConverters.JsonReformat(vJson, True);
    memJson.Lines.Clear;
    memJson.Lines.Add(vJson)
  except
    on e: Exception do
    begin
      ShowMessage('JSON Invalid, please chek!');
      Exit;
    end;
  end;

  try
    if jm = nil then
      jm := TPkgJsonMapper.Create();

    jm.ExportClass := TExporClass(grpConfig.ItemIndex);

    jm.DestinationUnitName := 'RootUnit';
    jm.Parse(memJson.Text, 'Root');

    frmGenerateUnit.synx.Text := jm.GenerateUnit;
    frmGenerateUnit.ShowModal();
  except
    on e: Exception do
    begin
      ShowMessage('JSON Invalid, please chek. </br></br>'+ e.Message);
    end;
  end;
end;

procedure TMainForm.DefineRegrasLayout;
begin
  AlinhamentoCenter;

  lblDoacao.Caption :=
  '<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_blank"> '+
  '<input type="hidden" name="cmd" value="_s-xclick"> '+
  '<input type="hidden" name="hosted_button_id" value="Z3JZ3GU9SGCLU"> '+
  '<input type="image" src="https://www.paypalobjects.com/pt_BR/BR/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - A maneira f�cil e segura de enviar pagamentos online!"> '+
  '<img alt="" border="0" src="https://www.paypalobjects.com/pt_BR/i/scr/pixel.gif" width="1" height="1"> '+
  '</form>';

  lblVersion.Caption := TSistema.GetVersao(UniServerModule.StartPath + '\versionoldjsontodelphi.dll');

  lblJsonToPascal.Visible := False;
end;

procedure TMainForm.lbl7Click(Sender: TObject);
begin
  UniApplication.UniSession.UrlRedirect('https://jsontodelphi.com');
end;

procedure TMainForm.UniFormAfterShow(Sender: TObject);
begin
  DefineRegrasLayout;
  UniSession.AddJS('ga(''set'',''page'', ''/old_'+Self.Name+'.html'');');
  UniSession.AddJS('ga(''send'',''pageview'');');
end;

procedure TMainForm.UniFormAjaxEvent(Sender: TComponent; EventName: string;
  Params: TUniStrings);
begin
  if EventName = 'resize' then
  begin
    if Self.Width < 1024 then
      pnlMaster.Align := alClient
    else
    begin
      pnlMaster.Align := alNone;
      AlinhamentoCenter;
    end;
  end;
end;

procedure TMainForm.UniFormClose(Sender: TObject; var Action: TCloseAction);
begin
  if Assigned(jm) then
    FreeAndNil(jm);
end;

initialization
  RegisterAppFormClass(TMainForm);

end.
